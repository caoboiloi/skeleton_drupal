<?php
/**
 * @file
 * Contains \Drupal\hello_world\Controller\HelloController.
 */
namespace Drupal\test_module\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\test_module\Service\DatetimeSalutation;
use Symfony\Component\DependencyInjection\ContainerInterface;
// extends ControllerBase, which happens to provide some helper tools(such as the StringTranslationTrait,
// which I will explain later in Chapter 13, Internationalization and Languages)


// Drupal-9-Module-Development-3rd.pdf

class TestModuleController extends ControllerBase {

  /**
   * @var \Drupal\test_module\Service\DatetimeSalutation
   */
  protected $salutation;
  /**
   * TestModuleController constructor.
   *
   * @param \Drupal\test_module\Service\DatetimeSalutation $salutation
   */
  public function __construct(DatetimeSalutation $salutation) {
    $this->salutation = $salutation;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('test_module.salutation')
    );
  }


  public function content() {
    $service_statically = \Drupal::service('test_module.salutation');
    return array(
      '#type' => 'markup',
      '#markup' =>  $this->salutation->getSalutation(),
    );
  }

  // EventSubscriber that takes this array, runs it through the Drupal theme layer, and returns the HTML page as a response
  public function test() {
    return array(
      '#type' => 'markup',
      '#markup' => t('chào lợi'),
    );
  }
}
