<?php
/**
 * @file
 * Contains \Drupal\hello_world\Controller\HelloController.
 */
namespace Drupal\test_module\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\test_module\Service\DatetimeSalutation;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
// extends ControllerBase, which happens to provide some helper tools(such as the StringTranslationTrait,
// which I will explain later in Chapter 13, Internationalization and Languages)


// Drupal-9-Module-Development-3rd.pdf

class TestModuleController extends ControllerBase {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;
  /**
   * SalutationConfigurationForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * The factory for configuration objects.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   * The logger.
   */
  // public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelInterface $logger) {
  //   $this->logger = $logger;
  // }
  // /**
  //  * {@inheritdoc}
  //  */
  // public static function create(ContainerInterface $container) {
  //   return new static(
  //     $container->get('config.factory'),
  //     // $container->get('test_module.salutation')
  //     $container->get('test_module.logger.mail_logger'),
  //   );
  // }


  public function content() {
    $service_statically = \Drupal::service('test_module.salutation');
    return array(
      '#type' => 'markup',
      // '#markup' =>  $this->salutation->getSalutation(),
    );
  }

  // EventSubscriber that takes this array, runs it through the Drupal theme layer, and returns the HTML page as a response
  public function test() {
    \Drupal::logger('test_module')->info('This is my error message');
    return array(
      '#type' => 'markup',
      // '#markup' => 'test',
      '#markup' => 'qwwdqweq',
      // '#markup' => $this->logger->info('The test module salutation has been changed to loi.'),
    );
  }
}
