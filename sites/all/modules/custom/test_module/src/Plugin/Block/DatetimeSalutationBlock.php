<?php

namespace Drupal\test_module\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\test_module\Service\DatetimeSalutation;
/**
 * Datetime salutation block.
 *
 * @Block(
 * id = "test_module_salutation_block",
 * admin_label = @Translation("Datetime salutation"),
 * )
 */
class DatetimeSalutationBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * The salutation service.
  *
  * @var \Drupal\test_module\Service\DatetimeSalutation
  */
  protected $salutation;
  /**
  * Constructs a DatetimeSalutationBlock.
  */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DatetimeSalutation $salutation) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->salutation = $salutation;
  }
    /**
    * {@inheritdoc}
    */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('test_module.salutation')
    );
  }
  /**
  * {@inheritdoc}
  */
  public function build() {
    return [
      '#markup' => $this->salutation->getSalutation().'- datetime block',
    ];
  }
}
